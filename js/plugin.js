(function( $ ) {
	$.fn.generate_jquery_ati_menu = function (){
		var lista = config.links;
		var menu = $("<ul id='menu' class='list-group'></ul>")
		menu.appendTo("#lista_desplegable");

		for (var i = lista.length - 1; i >= 0; i--) {
			var item;
			if(!Array.isArray(lista[i])) {
				item = $("<li class='list-group-item "+String(config.css_class)+"'><a href='"+lista[i].href+"' target='_blank'>"+lista[i].text+"</a></li>");
				item.css("color",config.color);
			} else {
				item = $("<li class='list-group-item deslizar "+String(config.css_class)+"'><a href='"+lista[i][0].href+"' target='_blank'>"+lista[i][0].text+"</a></li>");
				item.css("color",config.color);
				item.append("<ul class='list-group dropdown-menu' id='"+i+"'></ul>");	
			}
			item.appendTo("#menu");
		}

		$("#menu > .deslizar").mouseover( function () {
			var menu = $(this).find("ul");
			if(menu.length>0) {
				var i = parseInt(menu[0].id, 10);
				var sub_menu = lista[i];
				var sub_items = [];
				for (var j = sub_menu.length - 1; j > 0; j--) {
					sub_items[j] = $("<li class='"+config.css_class+"'><a href='"+sub_menu[j].href+"' target='_blank'>"+sub_menu[j].text+"</a></li>");
					sub_items[j].css("color",config.color);
					menu.append(sub_items[j]);
				}
				menu.slideDown("slow", function() {
					$(this).children().animate({
						opacity:0.4
					},(config.duration/2)).animate({
						opacity:1
					},(config.duration/2));
				}).delay(config.duration).slideUp("slow", function() {
					$(this).children().remove();
				});
			}

		});
	}
} ( jQuery ));
$("#lista_desplegable").generate_jquery_ati_menu();