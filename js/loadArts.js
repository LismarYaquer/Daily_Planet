var min = 0;
var page;


function loadArticules(content){
    var max = min+5;
	if(max>content.length) {
		max = content.length;
		min = 0;
	}
	content.slice(min,max).forEach(function(item){
        var newArt = document.createElement("articule");
        newArt.setAttribute("class", "col-xs-12 col-sm-8 col-md-9 col-lg-9 page-header pull-right");
        newArt.innerHTML = '<div class="row"><div class="col-xs-12 col-sm-12 col-sm-12 col-md-12"><div class="thumbnail"><div class="caption"><h3>'+item.titulo+'</h3><p>'+item.resumen+'</p><p><a href="articulo.html" target="_blank" class="btn btn-primary" role="button" onclick="">Ver</a> <a href="#" class="btn btn-default" role="button">Button</a></p></div></div></div></div>';
        document.getElementById("content").appendChild(newArt);
	});
	min = max;
}
