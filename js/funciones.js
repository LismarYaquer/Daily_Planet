$("#login").click(function(){
    $("#login-form").removeAttr("hidden");
    $("#signin").removeClass("active");
    $("#login").attr("class","active");
    $("#signin-form").attr("hidden", "true");
});
$("#signin").click(function(){
    $("#signin-form").removeAttr("hidden");
    $("#login").removeClass("active");
    $("#signin").attr("class","active");
    $("#login-form").attr("hidden", "true");
});

$( "#logout" ).click(function() {
	
	if (confirm("¿Está seguro de que desea cerrar sesión?")) {
		(window.location) = "index.html";
	}
});